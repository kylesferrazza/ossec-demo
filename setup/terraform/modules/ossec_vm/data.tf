data "vsphere_datacenter" "dc" {
  name = var.vc_datacenter
}

data "vsphere_network" "network" {
  name = var.vc_network
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_datastore" "datastore" {
  name = var.vc_datastore
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_resource_pool" "pool" {
  name = "${var.esxi_host}/Resources"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_virtual_machine" "ubuntu_20_template" {
  name = "ubuntu-20-templ"
  datacenter_id = data.vsphere_datacenter.dc.id
}
