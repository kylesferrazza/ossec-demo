terraform {
  backend "remote" {
    organization = "nuccdc"

    workspaces {
      name = "kyle-ossec-demo"
    }
  }
}

module "ossec_server" {
  source = "./modules/ossec_vm"
  vc_address = var.vc_address
  vc_username = var.vc_username
  vc_password = var.vc_password
  vc_datacenter = var.vc_datacenter
  vc_network = var.vc_network
  vc_datastore = var.vc_datastore
  esxi_host = var.esxi_host
  vm_name = "ossec_server"
  vm_ip = "192.168.110.200"
}

module "ossec_agent_one" {
  source = "./modules/ossec_vm"
  vc_address = var.vc_address
  vc_username = var.vc_username
  vc_password = var.vc_password
  vc_datacenter = var.vc_datacenter
  vc_network = var.vc_network
  vc_datastore = var.vc_datastore
  esxi_host = var.esxi_host
  vm_name = "ossec_agent_one"
  vm_ip = "192.168.110.201"
}

module "ossec_agent_two" {
  source = "./modules/ossec_vm"
  vc_address = var.vc_address
  vc_username = var.vc_username
  vc_password = var.vc_password
  vc_datacenter = var.vc_datacenter
  vc_network = var.vc_network
  vc_datastore = var.vc_datastore
  esxi_host = var.esxi_host
  vm_name = "ossec_agent_two"
  vm_ip = "192.168.110.202"
}
